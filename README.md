# Purpose
Repository is for openwrt linux distribution custom compilation and configuration.

Target hardware is tplink wrd3600
https://oldwiki.archive.openwrt.org/toh/tp-link/tl-wdr3600

# Build
Command to download openwrt source code and build openwrt image is
```
./build
```

Openwrt images will be created in path
```
$ ls -1 out/bin/targets/lantiq/xrx200/
config.seed
openwrt-imagebuilder-lantiq-xrx200.Linux-x86_64.tar.xz
openwrt-lantiq-xrx200-device-tplink-tdw8970.manifest
openwrt-lantiq-xrx200-tplink_tdw8970-squashfs-sysupgrade.bin
openwrt-sdk-lantiq-xrx200_gcc-7.3.0_musl.Linux-x86_64.tar.xz
openwrt-toolchain-lantiq-xrx200_gcc-7.3.0_musl.Linux-x86_64.tar.bz2
packages
sha256sums
```
INFO: When next time ./build command is run it will skip downloading source code and rebuilds only.

All dependencies must be installed in host system as described in
https://openwrt.org/docs/guide-developer/quickstart-build-images

## Docker

Alternatively for build process it could be used docker. First time it is needed to build docker image
```
./docker/build
```
Then run build in new docker container
```
./docker/run
```

To shell into such container
```
./docker/run bash
```

# Configuration

Build step run first time invokes fetch script which:
* fetch openwrt sources to out/ directory
* copies there extra file with differences of custom configuration as .config
* run "make defconfig" to expand specific custom configuration with all others default values

Then ./openwrt/out/.config can be additionally customized. To get these changes run
```
$ ( cd ./out ; ./scripts/diffconfig.sh > ../diffconfig )
```

# Flash

## Website

Openwrt image could be installed on target device by web gui usually available here http://192.168.1.1.

https://openwrt.org/docs/guide-quick-start/sysupgrade.luci

Website could be accessed also from command line

```
./upgrade openwrt-upgrade-filepath
```

## Ssh

Upload image to device /tmp directory (which is ram) using nc, scp or pendrive. Then run on device
```
sysupgrade -v /tmp/filename-of-downloaded-sysupgrade.bin
```
INFO: Only the *-sysupgrade.bin version of the OpenWrt download image should be used for OpenWrt upgrades. The *-factory.bin file is for switching from the vendor-default firmware to OpenWrt and is not used for OpenWrt-to-OpenWrt upgrades.

https://openwrt.org/docs/guide-user/installation/generic.sysupgrade

https://openwrt.org/docs/techref/flash.layout

# Install

Optional command to install custom configuration on target device
```
./install
```
WARNING: Current configuration allows ssh logins only with keys, not passwords.
So before installation exchange your ssh key
```
ssh root@192.168.1.1 "tee -a /etc/dropbear/authorized_keys" < ~/.ssh/id_rsa.pub
```

WARNING: Current configuration requires plugged external disk or pendrive and boot hangs without it (FIXME).

# Extended root

External disk or pendrive can be used to extend internal openwrt flash storage and overlay this filesystem.

* Prepare pendrive on host machine:

WARGING: All data on disk will be lost.

```
./extroot/initialize /dev/sd<X>
```

* Insert pendrive into target openwrt device

* Install basic configuration which will use extroot since next reboot
```
./install
```

* Reboot target

* Install all extra software from working directory containing "packages" directory with all these packages

```
./extroot/install
```

# Recovery

Hard reboot and press few times wps button for 1 second. Then it should boot rescue image with open ssh access.

https://openwrt.org/docs/guide-user/troubleshooting/vendor_specific_rescue
