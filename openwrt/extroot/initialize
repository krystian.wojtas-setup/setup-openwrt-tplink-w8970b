#!/bin/sh

# External disk or pendrive can be used to extend internal openwrt flash storage and overlay this filesystem
# Script prepares such disk on host machine
#
# WARGING: All data on pendrive will be lost

# Stop on first failure
set -e

# Show each command
set -x

# Get pendrive disk to use
disk="$1"

# Ensure that disk is specified
if test -z "$disk"; then
    echo >&2 "ERROR: Usage: $0 <disk>"
    exit 1
fi

# Ensure that user realy want to use this disk and lose all theirs data
echo "WARNING: All data on disk $disk will be lost. Are you sure? (yes/no): "
read confirmation
if test "$confirmation" != "yes"; then
    exit 1
fi

# Create dos partition table and one primary partition ext type
{
    echo o # Create a new empty DOS partition table
    echo n # Add a new partition
    echo p # Primary partition
    echo 1 # Partition number
    echo   # First sector (Accept default: varies)
    echo   # Last sector (Accept default: varies)
    echo w # Write changes
} | fdisk "$disk"

# Create filesystem
mkfs.ext4 \
    \
    `# Use first partition` \
    "${disk}1" \
    \
    `# Set custom label` \
    -L \
        extroot \
